# Encasa

El website esta en src/Encasa.Web

# Encasa.Web

## Controllers

Ac� est� toda la API con los servicios para enviar y recibis datos en JSON.

## Model

En la carpeta Entities se defined todas las clases que son entidades de negocio de la aplicaci�n, por ejemplo: Encuestas.

En la carpeta ViewModels se guardan todas las clases que son contratos de servicio con las p�ginas en AngularJS.

## Repositories

Cada repositorio guarda las queries asociadas a cada Entidad de negocio (Tabla).

## Scripts

Todo el JavaScript de la aplicaci�n, esto se compila en un �nico archivo app.js de forma autom�tica por Grunt.

## wwwroot

Est� es la carpeta p�blica, todo lo que est� ac� puede ser accedido directamente por un navegador.
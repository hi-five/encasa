﻿angular.module('app', [
        // Angular modules 
        'ngRoute'

        // 3rd Party Modules

        // Custom modules 

]).config(function ($routeProvider) {
    $routeProvider
        .when('/', {
            templateUrl: 'templates/encuesta.html',
            controller: 'homeController'
        })
        .otherwise('/');
});
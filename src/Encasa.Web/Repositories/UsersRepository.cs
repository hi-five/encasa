﻿using System.Linq;
using Encasa.Web.Model.Entities;

namespace Encasa.Web.Repositories
{
    public class UsersRepository : Repository
    {
        public UsersRepository(EncasaContext context) : base(context) { }

        public void Add(User user)
        {
            Context.Users.Add(user);
        }

        public User[] ToList()
        {
            return Context.Users.ToArray();
        }
    }
}

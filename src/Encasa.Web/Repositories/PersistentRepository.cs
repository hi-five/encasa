﻿using Encasa.Web.Model.Entities;

namespace Encasa.Web.Repositories
{
    public class PersistentRepository : Repository
    {
        public PersistentRepository(EncasaContext context) : base(context) { }

        public void Persist()
        {
            Context.SaveChanges();
        }
    }
}

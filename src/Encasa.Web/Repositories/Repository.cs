﻿using Encasa.Web.Model.Entities;

namespace Encasa.Web.Repositories
{
    public abstract class Repository
    {
        public EncasaContext Context { get; set; }

        public Repository(EncasaContext context)
        {
            Context = context;
        }
    }
}

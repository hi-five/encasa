﻿using System.Linq;
using Encasa.Web.Model.Entities;

namespace Encasa.Web.Repositories
{
    public class EncuestasRepository : Repository
    {
        public EncuestasRepository(EncasaContext context) : base(context) { }

        public void Add(Encuesta encuesta)
        {
            Context.Encuestas.Add(encuesta);
        }

        public Encuesta Get(int id)
        {
            return (from encuesta in Context.Encuestas
                    where encuesta.Id == id
                    select encuesta).FirstOrDefault();
        }

        public Encuesta[] ToList()
        {
            return Context.Encuestas.ToArray();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Encasa.Web.Model.Entities
{
    [Table("Encuestas")]
    public class Encuesta
    {
        [Key]
        public int Id { get; set; }
    }
}

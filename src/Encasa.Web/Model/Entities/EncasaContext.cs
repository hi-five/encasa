﻿using Microsoft.Data.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Encasa.Web.Model.Entities
{
    public class EncasaContext : DbContext
    {
        public DbSet<User> Users { get; set; }
        public DbSet<Encuesta> Encuestas { get; set; }
    }
}

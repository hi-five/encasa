﻿using Encasa.Web.Model.Entities;
using Encasa.Web.Repositories;
using Microsoft.AspNet.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Encasa.Web.Controllers
{
    [Route("api/[controller]")]
    public class EncuestasController:Controller
    {
        public EncuestasRepository EncuestasRepository { get; set; }
        public PersistentRepository PersistentRepository { get; set; }

        public EncuestasController(EncuestasRepository encuestasRepository, PersistentRepository persistentRepository)
        {
            EncuestasRepository = encuestasRepository;
            PersistentRepository = persistentRepository;
        }

        // GET: api/values
        [HttpGet]
        public IEnumerable<object> Get()
        {
            return EncuestasRepository.ToList();
        }

        [HttpGet]
        public Encuesta Get(int id)
        {
            return EncuestasRepository.Get(id);
        }

        [HttpPost]
        public void Post([FromBody]Encuesta encuesta)
        {
             EncuestasRepository.Add(encuesta);
        }
    }
}

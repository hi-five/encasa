﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNet.Mvc;
using Encasa.Web.Model.Entities;
using Encasa.Web.Repositories;

// For more information on enabling Web API for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace Encasa.Web.Controllers
{
    [Route("api/[controller]")]
    public class UsersController : Controller
    {
        public UsersRepository UsersRepository { get; set; }
        public PersistentRepository PersistentRepository { get; set; }

        public UsersController(UsersRepository usersRepository, PersistentRepository persistentRepository)
        {
            UsersRepository = usersRepository;
            PersistentRepository = persistentRepository;
        }

        // GET: api/values
        [HttpGet]
        public IEnumerable<object> Get()
        {
            return UsersRepository.ToList();
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public object Get(int id)
        {
            return "value";
        }

        // POST api/values
        [HttpPost]
        public void Post([FromBody]string value)
        {
          
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
